package dev.antoniodvr.covid19tracker.service;

import dev.antoniodvr.covid19tracker.model.Auditable;
import dev.antoniodvr.covid19tracker.model.CountryData;
import dev.antoniodvr.covid19tracker.model.ProvinceData;
import dev.antoniodvr.covid19tracker.model.RegionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Service
public class Covid19Service {

    private static final Logger logger = LoggerFactory.getLogger(Covid19Service.class);

    private Map<Integer, List<CountryData>> countryData;
    private Map<Integer, List<RegionData>> regionData;
    private Map<Integer, List<ProvinceData>> provinceData;

    private DPCClient dpcClient;
    public Covid19Service(DPCClient dpcClient) {
        this.dpcClient = dpcClient;
    }

    @PostConstruct
    @Scheduled(cron = "0 0 0/1 ? * *")
    public void init() {
        logger.info("Reading covid19 data started");

        this.countryData = Collections.singletonMap(380, dpcClient.getCountryData() // ISO 3166-1 numeric: 380 is Italy
                .sorted(Comparator.comparing(CountryData::getDate).reversed())
                .collect(toList()));
        logger.info("Read covid19 country data: {} items", this.countryData.size());

        this.regionData = dpcClient.getRegionData()
                .collect(
                        Collectors.groupingBy(
                                RegionData::getRegionCode,
                                LinkedHashMap::new,
                                collectingAndThen(
                                        toList(),
                                        l -> l.stream()
                                                .sorted(Comparator.comparing(RegionData::getDate).reversed())
                                                .collect(toList())
                                )
                        )
                );
        logger.info("Read covid19 region data: {} items", this.regionData.size());

        this.provinceData = dpcClient.getProvinceData()
                .peek(p -> {
                    if("".equals(p.getProvinceShortName())) {
                        p.setProvinceName("Non ancora assegnati");
                    }
                })
                .collect(
                        Collectors.groupingBy(
                                ProvinceData::getRegionCode,
                                LinkedHashMap::new,
                                collectingAndThen(
                                        toList(),
                                        l -> l.stream()
                                                .sorted(Comparator.comparing(ProvinceData::getDate).reversed())
                                                .collect(toList())
                                )
                        )
                );
        logger.info("Read covid19 province data: {} items", this.provinceData.size());
        logger.info("Reading covid19 data completed");
    }

    public Map<Integer, List<CountryData>> getCountryData() {
        return this.countryData;
    }

    public Map<Integer, List<RegionData>> getRegionData() {
        return regionData;
    }

    public Map<Integer, List<ProvinceData>> getProvinceData() {
        return provinceData;
    }

    public static <T extends Auditable> List<T> getLastDaysData(Collection<T> collection, int days) {
        return collection.stream()
                .sorted(Comparator.comparing(T::getDate))
                .skip(Math.max(0, collection.size() - days))
                .collect(toList());
    }

}

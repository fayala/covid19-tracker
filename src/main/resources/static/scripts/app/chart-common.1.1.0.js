/*
 |--------------------------------------------------------------------------
 | Shards Dashboards: Blog Overview Template
 |--------------------------------------------------------------------------
 */
'use strict';

(function($) {
    $(document).ready(function() {

        // Blog overview date range init.
        $('#blog-overview-date-range').datepicker({});

        //
        // Small Stats
        //

        // Datasets
        var boSmallStatsDatasets = [{
                backgroundColor: 'rgba(41,128,185,0.1)',
                borderColor: 'rgb(41,128,185)',
                data: totalCasesSeries,
            },
            {
                backgroundColor: 'rgba(196,24,44,0.1)',
                borderColor: 'rgb(196,24,44)',
                data: currentPositiveCasesSeries
            },
            {
                backgroundColor: 'rgba(0,0,0,0.1)',
                borderColor: 'rgb(0,0,0)',
                data: deathCasesSeries
            },
            {
                backgroundColor: 'rgba(23,198,113,0.1)',
                borderColor: 'rgb(23,198,113)',
                data: recoveredCasesSeries
            },
            {
                backgroundColor: 'rgb(196,24,44,0.1)',
                borderColor: 'rgb(196,24,44)',
                data: intensiveCareCasesSeries
            },
            {
                backgroundColor: 'rgb(230,126,34,0.1)',
                borderColor: 'rgb(230,126,34)',
                data: hospitalisedWithSymptomsCasesSeries
            },
            {
                backgroundColor: 'rgb(0,123,255,0.1)',
                borderColor: 'rgb(0,123,255)',
                data: homeConfinementCasesSeries
            }
        ];

        // Options
        function boSmallStatsOptions(max) {
            return {
                maintainAspectRatio: true,
                responsive: true,
                // Uncomment the following line in order to disable the animations.
                // animation: false,
                legend: {
                    display: false
                },
                tooltips: {
                    enabled: false,
                    custom: false
                },
                elements: {
                    point: {
                        radius: 0
                    },
                    line: {
                        tension: 0.3
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: false,
                        scaleLabel: false,
                        ticks: {
                            display: false
                        }
                    }],
                    yAxes: [{
                        gridLines: false,
                        scaleLabel: false,
                        ticks: {
                            display: false,
                            // Avoid getting the graph line cut of at the top of the canvas.
                            // Chart.js bug link: https://github.com/chartjs/Chart.js/issues/4790
                            suggestedMax: max
                        }
                    }],
                },
            };
        }

        // Generate the small charts
        boSmallStatsDatasets.map(function(el, index) {
            var chartOptions = boSmallStatsOptions(Math.max.apply(Math, el.data) + 1);
            var ctx = document.getElementsByClassName('blog-overview-stats-small-' + (index + 1));
            new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ["Label 1", "Label 2", "Label 3", "Label 4", "Label 5", "Label 6", "Label 7"],
                    datasets: [{
                        label: 'Today',
                        fill: 'start',
                        data: el.data,
                        backgroundColor: el.backgroundColor,
                        borderColor: el.borderColor,
                        borderWidth: 1.5,
                    }]
                },
                options: chartOptions
            });
        });


        //
        // Positive Recovered Death Cases
        //
        var positiveRecoveredDeath = document.getElementsByClassName('positive-recovered-death-cases')[0];
        new Chart(positiveRecoveredDeath, {
            type: 'bar',
            data: {
                labels: timeSeries,
                datasets: [{
                    label: currentPositiveLabel,
                    data: currentPositiveCasesSeries,
                    backgroundColor: 'rgba(196,24,44,0.9)',
                }, {
                    label: recoveredLabel,
                    data: recoveredCasesSeries,
                    backgroundColor: 'rgba(23,198,113,0.9)',
                }, {
                    label: deathLabel,
                    data: deathCasesSeries,
                    backgroundColor: 'rgba(0,0,0,0.7)',
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                    labels: {
                        boxWidth: 10
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: false,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            suggestedMax: 10,
                            callback: function(tick, index, ticks) {
                                if (tick === 0) {
                                    return tick;
                                }
                                // Format the amounts using Ks for thousands.
                                return tick > 999 ? (tick / 1000).toFixed(1) + 'K' : tick;
                            }
                        }
                    }]
                }
            }
        });

        //
        // Positive by situation
        //
        var positiveByTreatment = document.getElementsByClassName('positive-by-treatment')[0];
        new Chart(positiveByTreatment, {
            type: 'bar',
            data: {
                labels: timeSeries,
                datasets: [
                    {
                        label: intensiveCareLabel,
                        data: intensiveCareCasesSeries,
                        backgroundColor: 'rgba(196,24,44,0.9)',
                    },
                    {
                        label: hospitalisedWithSymptomsLabel,
                        backgroundColor: 'rgba(230,126,34,0.7)',
                        data: hospitalisedWithSymptomsCasesSeries
                    },
                    {
                        label: homeConfinementLabel,
                        backgroundColor: 'rgba(41,128,185,0.5)',
                        data: homeConfinementCasesSeries
                    }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                    labels: {
                        boxWidth: 10
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: false,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            suggestedMax: 10,
                            callback: function(tick, index, ticks) {
                                if (tick === 0) {
                                    return tick;
                                }
                                // Format the amounts using Ks for thousands.
                                return tick > 999 ? (tick / 1000).toFixed(1) + 'K' : tick;
                            }
                        }
                    }]
                }
            }
        });

        //
        // Current New Positive Cases
        //
        var currentNewPositiveCtx = document.getElementsByClassName('current-new-positive-cases')[0];
        new Chart(currentNewPositiveCtx, {
            type: 'bar',
            data: {
                labels: timeSeries,
                datasets: [{
                    label: currentNewPositiveLabel,
                    data: currentNewPositiveCasesSeries,
                    backgroundColor: 'rgba(196,24,44,0.9)',
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false,
                    position: 'top'
                },
                scales: {
                    xAxes: [{
                        gridLines: false,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMax: 10,
                            callback: function(tick, index, ticks) {
                                if (tick === 0) {
                                    return tick;
                                }
                                // Format the amounts using Ks for thousands.
                                return tick > 999 ? (tick / 1000).toFixed(1) + 'K' : tick;
                            }
                        }
                    }]
                }
            }
        });


        //    new Chart(newPositiveByRegion, {
        //      type: 'pie',
        //      data: {
        //          datasets: [{
        //            hoverBorderColor: '#ffffff',
        //            data: newPositiveByRegionSeries,
        //            backgroundColor: [
        //              'rgba(196,24,44,0.9)',
        //              'rgba(230,126,34,0.7)',
        //              'rgba(41,128,185,0.5)',
        //              'rgba(0,0,0,0.7)',
        //              'rgba(23,198,113,0.7)'
        //            ]
        //          }],
        //          labels: newPositiveByRegionLabels
        //        },
        //      options: {
        //         legend: {
        //           position: 'bottom',
        //           labels: {
        //             boxWidth: 10
        //           }
        //         },
        //         cutoutPercentage: 50,
        //         // Uncomment the following line in order to disable the animations.
        //         // animation: false,
        //       }
        //    });

    });
})(jQuery);
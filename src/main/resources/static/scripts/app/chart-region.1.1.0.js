'use strict';

(function($) {
    $(document).ready(function() {

        //
        // Total cases by province
        //
        var totalCasesByProvince = document.getElementsByClassName('total-cases-by-province')[0];
        new Chart(totalCasesByProvince, {
            type: 'bar',
            data: {
                labels: totalCasesByProvinceLabels,
                datasets: [
                    {
                        label: previousReportsLabel,
                        data: totalCasesByProvinceSeries,
                        backgroundColor: 'rgba(196,24,44,0.7)',
                    },
                    {
                        label: lastReportLabel,
                        data: totalNewCasesByProvinceSeries,
                        backgroundColor: 'rgba(196,24,44,0.9)',
                    }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top'
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: false,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            suggestedMax: 10,
                            callback: function(tick, index, ticks) {
                                if (tick === 0) {
                                    return tick;
                                }
                                // Format the amounts using Ks for thousands.
                                return tick > 999 ? (tick / 1000).toFixed(1) + 'K' : tick;
                            }
                        }
                    }]
                }
            }
        });
    });
})(jQuery);